function getInfo() {
    let firstname  = document.getElementById("fname").value;
    let lastname = document.getElementById("lname").value;
    let email = document.getElementById("email").value;
    let pword = document.getElementById("password").value;
    let confirmpassword = document.getElementById("cPassword").value;

    if(firstname == "" || lastname == ""  || email == ""  || password == ""  || confirmpassword == "" ) {
        console.log("Please fill in your information");
    } else if (pword.length < 8 || pword == "") {
        console.log("Password length is less than 8 characters");
    } else if (pword != confirmpassword.value){
        console.log ("passwords does not match. Check Password Fields");
    } else {
        console.log("Thanks for logging your information");
    }
}
